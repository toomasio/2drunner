﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(Pool))]
public class PoolEditor : Editor
{
    private int size;

    SerializedObject sourceRef;
    SerializedProperty poolObjectsRef;
    private Pool source;

    private GameObject nextPrefab;
    GUIStyle textStyle = new GUIStyle();

    // Use this for initialization
    void OnEnable()
    {
        source = (Pool)target;
        sourceRef = new SerializedObject(source);
        poolObjectsRef = sourceRef.FindProperty("poolObjects");

        textStyle.alignment = TextAnchor.MiddleCenter;
        textStyle.fontStyle = FontStyle.Bold;
	}

    public override void OnInspectorGUI()
    {
        sourceRef.Update();

        LoadSpawnableList();
        DragObjects();

        sourceRef.ApplyModifiedProperties();
    }

    void LoadSpawnableList()
    {
        size = poolObjectsRef.arraySize;
        if (size != poolObjectsRef.arraySize)
        {
            while (size > poolObjectsRef.arraySize)
            {
                poolObjectsRef.InsertArrayElementAtIndex(poolObjectsRef.arraySize);
            }
            while (size < poolObjectsRef.arraySize)
            {
                poolObjectsRef.DeleteArrayElementAtIndex(poolObjectsRef.arraySize - 1);
            }
            
        }

        //display lists
        for (int i = 0; i < source.poolObjects.Count; i++)
        {
            //change name of category if go updated
            var curPrefab = source.poolObjects[i].prefabToSpawn;
            if (curPrefab)
            {
                if (curPrefab.name != source.poolObjects[i].name)
                    source.poolObjects[i].name = curPrefab.name;
            }
            

            EditorGUILayout.BeginHorizontal();//keep on one row

            //get properties reference
            SerializedProperty listRef = poolObjectsRef.GetArrayElementAtIndex(i);
            //display properties
            EditorGUILayout.PropertyField(listRef, true);
            //delete button to remove a property
            if (GUILayout.Button("DELETE"))
            {
                RemoveSpawnableObject(i);
            }

            EditorGUILayout.EndHorizontal();
        }
        
    }

    void DragObjects()
    {
        EditorGUILayout.BeginHorizontal();
        Event evt = Event.current;
        Rect drop_area = GUILayoutUtility.GetRect(0.0f, 30.0f, GUILayout.ExpandWidth(true));

        GUI.Box(drop_area,"");
        GUI.TextArea(drop_area, "Add Prefab To Pool (Drag)", textStyle);

        
        if (GUILayout.Button("+"))
        {
            AddSpawnableObject(null);
        }
        EditorGUILayout.EndHorizontal();

        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!drop_area.Contains(evt.mousePosition))
                    return;

                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                if (evt.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();

                    foreach (Object dragged_object in DragAndDrop.objectReferences)
                    {
                        //check if prefab
                        if (PrefabUtility.GetPrefabParent(dragged_object) == null && PrefabUtility.GetPrefabObject(dragged_object) != null)
                        {
                            GameObject spawn = (GameObject)dragged_object;
                            AddSpawnableObject(spawn);
                        }
                    }
                }
                break;
        }
    }

    void AddSpawnableObject(GameObject _spawn)
    {
        source.poolObjects.Add(new Pool.SpawnableObject());
        if (!_spawn)
            return;

        int i = source.poolObjects.Count - 1;
        source.poolObjects[i].prefabToSpawn = _spawn;

    }

    void RemoveSpawnableObject(int _ind)
    {
        source.poolObjects.RemoveAt(_ind);
    }

}
