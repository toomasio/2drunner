﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    private LevelManager lm;
	// Use this for initialization
	void Start ()
    {
        //getcomps
        lm = GameManager.instance.GetLevelManager();
	}
	
	public void Restart()
    {
        lm.RestartScene();
    }

    public void Quit()
    {
        lm.QuitGame();
    }
}
