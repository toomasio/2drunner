﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Aiming : MonoBehaviour 
{

	[SerializeField]
	private float debugRad = 1;
	[SerializeField]
	private Transform player;

	private Vector3 pos;
	private LineRenderer line;
	void Start()
	{
		line = GetComponent<LineRenderer> ();
	}

	void FixedUpdate()
	{
		GetMousePos ();
		DrawLines ();
	}

	void DrawLines()
	{
		line.startColor = Color.red;
		line.endColor = Color.red;
		line.SetPosition (0, player.position);
		line.SetPosition (1, pos);
	}

	void GetMousePos()
	{
		Vector3 mouse = Input.mousePosition;
		mouse.z = -Camera.main.transform.position.z;
		pos = Camera.main.ScreenToWorldPoint(mouse);
	}

	void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere (pos,debugRad);
	}
}
