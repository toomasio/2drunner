﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDespawner : MonoBehaviour
{
    [SerializeField]
    private float lifeTime = 1;
    private float timer;

    private Pool pool;

	// Use this for initialization
	void Start ()
    {
        pool = GameManager.instance.GetPool();
	}
	
	// Update is called once per frame
	void Update ()
    {
        timer += Time.deltaTime;
        if (timer > lifeTime)
        {
            pool.UnspawnObject(this.gameObject);
            timer = 0;
        }
	}
}
