﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupHealth : MonoBehaviour
{
    [SerializeField]
    private int healthAmount = 1;
    [SerializeField]
    private GameObject pickUpFXPrefab;

    private Pool pool;

    void Start()
    {
        pool = GameManager.instance.GetPool();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            Player pl = col.transform.root.GetComponentInChildren<Player>();
            AwardHealth(pl);
        }
    }

    void AwardHealth(Player _player)
    {
        _player.AddHealth(healthAmount);
        KillObject();
    }

    void KillObject()
    {
        Destroy(gameObject);

        if (pickUpFXPrefab)
            pool.SpawnObject(pickUpFXPrefab, transform.position, Quaternion.identity);
    }
}
