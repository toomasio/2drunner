﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private float maxSpeed = 6;
    [SerializeField]
    private float speedLerpTime = 1;
    private float curSpeed;
    [SerializeField]
    private bool enableDoubleJump = true;
    [SerializeField]
    private float jumpForce = 6;
    private bool jump;
    [SerializeField]
    private bool enablePowerJump = true;
    [SerializeField]
    private float powerJumpForce = 10;
    [SerializeField]
    private float powerJumpSens = 0.5f;
    [SerializeField]
    private float powerJumpMaxHoldTime = 1;
    [SerializeField]
    private float powerJumpCoolDown = 5;
    private float powerjumpTimer;
    private bool powerJumping;
    private bool powerJumpCool = true;
    [SerializeField]
    private bool enableSlide = true;
    [SerializeField]
    private float slidePower = 5;
    private float slideTime = 1;
    private float slideHeightPerc = 0.5f;
    private bool sliding;
    [SerializeField]
    private float gravityMultiplier;
    [SerializeField]
    private float lowJumpMultiplier;
    [SerializeField]
    private LayerMask groundMask;
    [SerializeField]
    private Vector2 groundBoxSize = Vector2.one;
    [SerializeField]
    private Vector2 groundBoxCenter = Vector2.one;
    private bool grounded;
    [SerializeField]
    private LayerMask wallMask;
    [SerializeField]
    private Vector2 leftDetectSize = Vector2.one;
    [SerializeField]
    private Vector2 leftDetectCenter = Vector2.left;
    [SerializeField]
    private Vector2 rightDetectSize = Vector2.one;
    [SerializeField]
    private Vector2 rightDetectCenter = Vector2.right;
    [SerializeField]
    private float wallBounceForceX = 5;
    [SerializeField]
    private float wallBounceForceY = 5;
    private bool wallHitLeft;
    private bool wallHitRight;
    [SerializeField]
    private bool disableMovement;

    private bool facingRight = true;
    private float playerSpeed;

    private Rigidbody2D rb;
    private bool doubleActive;
    private bool doubleJump;
    private PlayerAnimations anim;
    private Player player;
    private PlayerUI plUI;
    private Transform colliderTrans;
    // Use this for initialization
    void Start()
    {
        //get comps
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<PlayerAnimations>();
        player = GetComponent<Player>();
        plUI = GameManager.instance.GetPlayerUI();
        colliderTrans = GetComponentInChildren<CapsuleCollider2D>().transform;

        StartCoroutine(StartSpeedLerp());
        StartCoroutine(CalcPlayerSpeed());
    }

    void Update()
    {
        if (GameManager.instance.IsWin() || GameManager.instance.IsLose())
            return;

        GetInputs();
        CheckGrounded();
        CheckDirection();
        CheckWallHits();
        SyncAnimations();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameManager.instance.IsWin() || GameManager.instance.IsLose() || powerJumping || player.IsDead())
            return;

        MovePlayer();
        CheckHeightVelocity();
    }

    void GetInputs()
    {
        jump = Input.GetButtonDown("Jump");
        if (jump)
        {
            Jump();
        }
            
        if (Input.GetButton("Fire1"))
        {
            PowerJump();
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            ResetPowerJump();
        }

        if (Input.GetButtonDown("Fire2"))
        {
            Slide();
        }
    }

    void CheckHeightVelocity()
    {
        if (rb.velocity.y < 0)
            AddGravity(gravityMultiplier);
        if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
            AddGravity(lowJumpMultiplier);
            
    }

    void AddGravity(float _multiplier)
    {
        rb.velocity += Vector2.up * Physics2D.gravity.y * _multiplier * Time.deltaTime;
    }

    void Slide()
    {
        if (enableSlide && grounded && !sliding)
        {
            StartCoroutine(StartSlide());
        }
    }

    void PowerJump()
    {
        if (!enablePowerJump || !powerJumpCool)
            return;

        powerjumpTimer += Time.deltaTime;
        if (powerjumpTimer > powerJumpSens)
        {
            if (!powerJumping)
            {
                StartCoroutine(StartPowerJump());
            }
        }
    }

    void ResetPowerJump()
    {
        powerJumping = false;
        powerjumpTimer = 0;
    }

    IEnumerator StartPowerJump()
    {
        powerJumping = true;
        float timer = 0;
        while (Input.GetButton("Fire1") && timer < powerJumpMaxHoldTime)
        {
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        Jump();
        ResetPowerJump();
        StartCoroutine(StartPowerJumpCoolDown());
        StartCoroutine(StartSpeedLerp());
    }

    IEnumerator StartPowerJumpCoolDown()
    {
        powerJumpCool = false;
        float timer = 0;
        float perc = 0;
        while (perc < 1)
        {
            timer += Time.deltaTime;
            perc = timer / powerJumpCoolDown;
            //sync to UI
            plUI.SetPowerJumpCoolDown(perc);
            yield return new WaitForEndOfFrame();
        }
        powerJumpCool = true;
    }

    IEnumerator StartSlide()
    {
        sliding = true;
        //slide player
        rb.velocity = Vector2.right * slidePower;
        //scale collider down
        colliderTrans.localScale = new Vector3(colliderTrans.localScale.x, colliderTrans.localScale.y * slideHeightPerc,
            colliderTrans.localScale.z);
        //start timer to slow down player
        float timer = 0;
        float perc = 0;
        while (perc < 1)
        {
            timer += Time.deltaTime;
            perc = timer / slideTime;
            //slow player down during slide time
            curSpeed = Mathf.Lerp(0, maxSpeed, perc);
            yield return new WaitForEndOfFrame();
        }
        //revert collider size
        colliderTrans.localScale = Vector3.one;
        sliding = false;
    }

    void CheckDirection()
    {
        //make sure player is always moving right when grounded
        if (grounded && !facingRight && !jump)
            FlipPlayer();
    }

    void CheckGrounded()
    {
        //ground detect
        grounded = Physics2D.OverlapBox((Vector2)transform.position + groundBoxCenter, groundBoxSize, 0, groundMask);
    }

    void CheckWallHits()
    {
        //left detect        
        wallHitLeft = Physics2D.OverlapBox((Vector2)transform.position + leftDetectCenter, leftDetectSize, 0, wallMask);
        //right detect
        wallHitRight = Physics2D.OverlapBox((Vector2)transform.position + rightDetectCenter, rightDetectSize, 0, wallMask);
    }

    public void ResetSpeed()
    {
        StartCoroutine(StartSpeedLerp());
    }

    IEnumerator StartSpeedLerp()
    {
        float timer = 0;
        curSpeed = 0;
        while (timer < speedLerpTime)
        {
            timer += Time.deltaTime;
            if (timer > speedLerpTime)
                timer = speedLerpTime;
            float perc = timer / speedLerpTime;
            curSpeed = Mathf.Lerp(0, maxSpeed, perc);

            yield return new WaitForEndOfFrame();
        }
    }

    void MovePlayer()
    {
        if (disableMovement)
            return;

        Vector3 vel = Vector3.forward * curSpeed * Time.fixedDeltaTime;
        transform.Translate(vel);
    }

    void Jump()
    {
        if (grounded || doubleActive || powerJumping || wallHitLeft || wallHitRight)
        {
            Vector2 force = Vector2.up * jumpForce;
            //wall jumps
            if (wallHitLeft && !powerJumping)
            {
                if (!facingRight)
                    FlipPlayer();
                force = (Vector2.right * wallBounceForceX) + (Vector2.up * wallBounceForceY);
            }
            else if (wallHitRight && !powerJumping)
            {
                if (facingRight)
                    FlipPlayer();
                force = (Vector2.left * wallBounceForceX) + (Vector2.up * wallBounceForceY);
            }
            else if (powerJumping)
                force = Vector2.up * powerJumpForce;


            //jump
            rb.velocity = force;
            if (grounded && enableDoubleJump)
                doubleActive = true;
            else if (doubleActive)
            {
                StartCoroutine(StartDoubleJumpBool());
                doubleActive = false;
            }
                
        }

    }

    IEnumerator StartDoubleJumpBool()
    {
        doubleJump = true;
        yield return new WaitForEndOfFrame();
        doubleJump = false;
    }

    public void BounceOff(Vector3 _pos, float _force)
    {
        Vector3 direction = (transform.position - _pos).normalized;
        Vector2 forceDir = Vector2.left;
        //bounce player left or right consistently based on direction
        if (direction.x >= 0)
            forceDir = Vector3.right;
        //only bounce up if player is above
        if (direction.y >= 0)
            forceDir = Vector3.up;
        //bounce player
        rb.velocity = (forceDir * _force);
        //set speed to zero for lerp
        ResetSpeed();
    }

    void FlipPlayer()
    {
        transform.Rotate(0, 180, 0);
        facingRight = !facingRight;
    }

    void OnDrawGizmosSelected()
    {

        //draw grounded gizmo
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube((Vector2)transform.position + groundBoxCenter, groundBoxSize);

        //draw wall gizmos
        Gizmos.DrawWireCube((Vector2)transform.position + leftDetectCenter, leftDetectSize);
        Gizmos.DrawWireCube((Vector2)transform.position + rightDetectCenter, rightDetectSize);

    }

    IEnumerator CalcPlayerSpeed()
    {
        while (Application.isPlaying)
        {
            // Position at frame start
            Vector3 lastPos = transform.position;
            // Wait till it the end of the frame
            yield return new WaitForFixedUpdate();
            // Calculate velocity: Velocity = DeltaPosition / DeltaTime
            playerSpeed = ((lastPos - transform.position) / Time.deltaTime).magnitude;
        }
    }

    void SyncAnimations()
    {

        anim.doubleJump = doubleJump;
        anim.jump = jump;
        anim.grounded = grounded;
        anim.chargingUp = powerJumping;
        anim.wallRight = wallHitRight;
        anim.wallLeft = wallHitLeft;
        anim.sliding = sliding;
        anim.powerJumping = powerJumping;
        anim.playerSpeed = playerSpeed;
    }

}
