﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupPoints : MonoBehaviour
{

    [SerializeField]
    private int points = 1;
    [SerializeField]
    private GameObject pickUpFXPrefab;

    private PlayerStats plStats;
    private Pool pool;

    void Start()
    {
        pool = GameManager.instance.GetPool();
        plStats = GameManager.instance.GetPlayerStats();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            AwardPoints();
        }
    }

    void AwardPoints()
    {
        plStats.AddPoints(points);
        KillObject();
    }

    void KillObject()
    {
        Destroy(gameObject);

        if (pickUpFXPrefab)
            pool.SpawnObject(pickUpFXPrefab,transform.position, Quaternion.identity);
    }
}
