﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePlayer : MonoBehaviour
{
    [SerializeField]
    private bool instantKill;
    [SerializeField]
    private int damage = 1;
    [SerializeField]
    private float bounceForce = 5;

    private Player player;

    void Start()
    {
        //get comp
        player = GameObject.FindGameObjectWithTag("Player").
            GetComponent<Player>();
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            if (!player)
                player = col.transform.root.GetComponentInChildren<Player>();

            DamagePlayerHealth();
        }
    }

    void DamagePlayerHealth()
    {
        if (instantKill)
            player.RemoveHealth(player.GetCurHealth(),transform.position, bounceForce);
        else
            player.RemoveHealth(damage, transform.position, bounceForce);
    }

}
