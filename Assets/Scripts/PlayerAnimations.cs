﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{

    [SerializeField]
    private Animator anim;
    public bool grounded;
    public bool jump;
    public bool chargingUp;
    public bool doubleJump;
    public bool wallRight;
    public bool wallLeft;
    public bool sliding;
    public bool powerJumping;
    public float playerSpeed;
    public bool dead;

    // Update is called once per frame
    void Update ()
    {
        anim.SetBool("grounded",grounded);
        anim.SetBool("jump", jump);
        anim.SetBool("chargingUp", chargingUp);
        anim.SetBool("doubleJump", doubleJump);
        anim.SetBool("wallRight", wallRight);
        anim.SetBool("wallLeft", wallLeft);
        anim.SetBool("sliding", sliding);
        anim.SetBool("powerJumping", powerJumping);
        anim.SetFloat("playerSpeed", playerSpeed);
        anim.SetBool("dead", dead);
    }
}
