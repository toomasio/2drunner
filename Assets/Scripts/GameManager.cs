﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField]
    private PlayerUI playerUI;
    [SerializeField]
    private Pool pool;
    [SerializeField]
    private PlayerStats playerStats;
    [SerializeField]
    private LevelManager levelManager;
    [SerializeField]
    private MenuManager menuManager;
    [SerializeField]
    private GameObject DeathScene;
    [SerializeField]
    private float DeathSceneDelay = 2;

    private bool win;
    private bool lose;
    private GameObject mainCam;

    public static GameManager instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        mainCam = Camera.main.gameObject;
    }

    public PlayerUI GetPlayerUI()
    {
        return playerUI;
    }

    public void WinGame()
    {
        win = true;

        //update UI
        playerUI.WinGame();
    }

    public void LoseGame()
    {
        lose = true;
        StartCoroutine(StartLoseGame());
    }

    IEnumerator StartLoseGame()
    {
        float timer = 0;
        while (timer < DeathSceneDelay)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        mainCam.SetActive(false);
        DeathScene.SetActive(true);
    }

    public bool IsWin()
    {
        return win;
    }

    public bool IsLose()
    {
        return lose;
    }

    public Pool GetPool()
    {
        return pool;
    }

    public PlayerStats GetPlayerStats()
    {
        return playerStats;
    }

    public LevelManager GetLevelManager()
    {
        return levelManager;
    }

    public MenuManager GetMenuManager()
    {
        return menuManager;
    }
	
}
