﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private int maxHealth = 5;
    private int curHealth;
    [SerializeField]
    private float damageCoolDown = 1;
    private bool damageImmune = false;

    private bool dead;

    private PlayerUI plUI;
    private PlayerController plController;
    private PlayerAnimations plAnim;

    // Use this for initialization
    void Start()
    {
        curHealth = maxHealth;

        //get comp
        plUI = GameManager.instance.GetPlayerUI();
        plController = GetComponent<PlayerController>();
        plAnim = GetComponent<PlayerAnimations>();

        //Update UI
		plUI.SetHealthBarValues (0, curHealth);
        plUI.UpdateHealth(curHealth);
    }

    public int GetCurHealth()
    {
        return curHealth;
    }

    public void AddHealth(int _amount)
    {
        curHealth += _amount;
        if (curHealth >= maxHealth)
            curHealth = maxHealth;

        //update UI
        plUI.UpdateHealth(curHealth);
    }

    public void RemoveHealth(int _amount, Vector3 _pos, float _bounceForce)
    {
        if (damageImmune || dead)
            return;

        curHealth -= _amount;

        //update UI
        plUI.UpdateHealth(curHealth);
        //bounce Player
        plController.BounceOff(_pos, _bounceForce);


        if (curHealth <= 0)
        {
            dead = true;
            LoseGame();
        }
        else
            //damage immune
            StartCoroutine(StartCoolDown());
    }

    IEnumerator StartCoolDown()
    {
        damageImmune = true;
        float timer = 0;
        while (timer < damageCoolDown)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        damageImmune = false;
    }

    void WinGame()
    {
        GameManager.instance.WinGame();
    }

    void LoseGame()
    {
        GameManager.instance.LoseGame();

        //update anim
        plAnim.dead = dead;
    }

    public bool IsDead()
    {
        return dead;
    }

}
