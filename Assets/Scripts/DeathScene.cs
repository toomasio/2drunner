﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathScene : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    [SerializeField]
    private Transform playerPosition;

    // Use this for initialization
    void OnEnable ()
    {
        player.position = playerPosition.position;
	}
	

}
