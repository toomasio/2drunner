﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    [SerializeField]
    private Vector3 offset;
    [SerializeField]
    private float followSens = 3;

	private Vector3 curOffset;
	private float startOrthoSize;
	private Camera playerCamera;

	void Start()
	{
		playerCamera = GetComponent<Camera> ();
		curOffset = offset;
		startOrthoSize = playerCamera.orthographicSize;
	}

    void FixedUpdate()
    {
        FollowPlayer();
    }

    void FollowPlayer()
    {
        transform.position = Vector3.Lerp(transform.position, player.position + curOffset, Time.fixedDeltaTime * followSens);
    }

	public void ChangeOffset(Vector3 _newOffset, float _orthoOffset, float _time)
	{
		StartCoroutine (ZoomCamera (_newOffset, _orthoOffset, _time));
	}

	public void ResetOffset(float _time)
	{
		StartCoroutine (ZoomCamera (offset, startOrthoSize, _time));
	}
		

	IEnumerator ZoomCamera(Vector3 _newOffset, float _orthoSize, float _time)
	{
		float timer = 0;
		float perc = 0;
		Vector3 startOffset = curOffset;
		float curSize = playerCamera.orthographicSize;
		while (perc < 1) 
		{
			timer += Time.fixedDeltaTime;
			perc = timer / _time;

			//lerp
			curOffset = Vector3.Lerp(startOffset, _newOffset,perc);
			playerCamera.orthographicSize = Mathf.Lerp(curSize, _orthoSize, perc);

			yield return new WaitForFixedUpdate ();
		}
	}

}
