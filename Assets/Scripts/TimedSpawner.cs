﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedSpawner : MonoBehaviour
{
    [SerializeField]
    private float intervalTime = 1;
    private float timer;
    private Pool pool;

    [SerializeField]
    private GameObject objToSpawn;

	// Use this for initialization
	void Start ()
    {
        pool = GameManager.instance.GetPool();
	}
	
	// Update is called once per frame
	void Update ()
    {
        timer += Time.deltaTime;
        if (timer > intervalTime)
        {
            pool.SpawnObject(objToSpawn, Vector3.zero, Quaternion.identity);
            timer = 0;
        }
	}
}
