﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    [SerializeField]
    private int maxPointsToWin = 0; // zero to disable
    private int curPoints;

    private PlayerUI plUI;

    // Use this for initialization
    void Start()
    {

        //get comp
        plUI = GameManager.instance.GetPlayerUI();

        //Update UI
        plUI.UpdatePoints(curPoints);
    }

    public void AddPoints(int _amount)
    {
        curPoints += _amount;

        //update UI
        plUI.UpdatePoints(curPoints);

        if (maxPointsToWin > 0 && curPoints >= maxPointsToWin)
        {
            WinGame();
        }
    }

    void WinGame()
    {
        GameManager.instance.WinGame();
    }

    void LoseGame()
    {
        GameManager.instance.LoseGame();
    }
}
