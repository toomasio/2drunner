﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{

    [SerializeField]
    private Text pointsText;
    [SerializeField]
    private Text healthText;
	[SerializeField]
	private Slider healthBar;
    [SerializeField]
    private GameObject winObject;
    [SerializeField]
    private GameObject loseObject;
    [SerializeField]
    private Image powerJumpCoolDown;

    public void SetPowerJumpCoolDown(float _value)
    {
        powerJumpCoolDown.fillAmount= 1 - _value;
    }

    public void UpdatePoints(int _amount)
    {
        pointsText.text = _amount.ToString();
    }

    public void UpdateHealth(int _amount)
    {
        healthText.text = _amount.ToString();

		if (healthBar)
			healthBar.value = _amount;
    }

	public void SetHealthBarValues(float _min, float _max)
	{
		if (!healthBar)
			return;
		
		healthBar.minValue = _min;
		healthBar.maxValue = _max;
	}

    public void WinGame()
    {
        winObject.SetActive(true);
    }

    public void LoseGame()
    {
        loseObject.SetActive(true);
    }

}
