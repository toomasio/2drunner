﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUIFollowPlayer : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    [SerializeField]
    private float followSens = 10;

	// Use this for initialization
	void Start ()
    {
        if (!player)
            player = GameObject.FindGameObjectWithTag("Player").transform;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        FollowPlayer();
	}

    void FollowPlayer()
    {
        transform.position = Vector3.Lerp(transform.position, player.position, Time.deltaTime * followSens);
    }
}
