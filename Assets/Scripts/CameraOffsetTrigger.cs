﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOffsetTrigger : MonoBehaviour 
{

	[SerializeField]
	private Vector3 newOffset;
	[SerializeField]
	private float orthoOffset;
	[SerializeField]
	private float zoomTime = 0.5f;

	private PlayerCamera playerCamera;

	void Start()
	{
		playerCamera = Camera.main.GetComponent<PlayerCamera> ();
	}

	void OnTriggerEnter2D(Collider2D _col)
	{
		if (_col.tag != "Player")
			return;

		playerCamera.ChangeOffset (newOffset, orthoOffset, zoomTime);
	}

	void OnTriggerExit2D(Collider2D _col)
	{
		if (_col.tag != "Player")
			return;
		
			playerCamera.ResetOffset(zoomTime);
	}
		
}
