﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSection : MonoBehaviour
{
    [SerializeField]
    private Transform endpoint;
    
    public Transform GetEndPoint()
    {
        return endpoint;
    }
}