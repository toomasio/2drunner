﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSectionSpawnManager : MonoBehaviour
{

    [System.Serializable]
    private class Level
    {
        public string name = "Level";
        public List<LevelSection> levelSections = new List<LevelSection>();
        public enum SpawnType {Sequence,Random}
        public SpawnType spawnType = SpawnType.Sequence;
        public bool timed = false;
        public float levelTime = 0;
        public LevelSection endObstacle = null;
    }

    [SerializeField]
    private Level[] levels;
    [SerializeField]
    private float spawnDistanceX = -30;
    [SerializeField]
    private float gameFinishDistance = -5;
    [SerializeField]
    private float destroyDistanceX = 200;
    [Header("Scene Start Section. NOT PREFAB")]
    [SerializeField]
	private LevelSection startLevelSection;

    private int curLevelInd;
	private int curSectInd = 0;
    private LevelSection curSpawn;
    private Vector3 curEndPos;
    private Vector3 curDestroyPos;

    private Transform player;
    private float curSpawnDistance;
    private float curDestroyDistance;
    private float timer;

    private bool levelsFinished;

    private Pool pool;

    private List<GameObject> obsToUnspawn = new List<GameObject>();
    private List<LevelSection> randomSpawns = new List<LevelSection>();

    // Use this for initialization
    void Start ()
    {
        //get comps
        pool = GameManager.instance.GetPool();

        player = GameObject.FindGameObjectWithTag("Player").transform;
        curLevelInd = 0;
        curEndPos = startLevelSection.GetEndPoint().position;
        curDestroyPos = startLevelSection.transform.position;

        //initialize destruction array
        obsToUnspawn.Add(startLevelSection.gameObject);

        if (levels[curLevelInd].timed)
            timer = levels[curLevelInd].levelTime;
            
    }
	
	// Update is called once per frame
	void Update ()
    {
        CheckPlayerDistance();

        if (levels[curLevelInd].timed)
            CountDown();
	}

    void CountDown()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            NextLevelSection();
        }
    }

    void CheckPlayerDistance()
    {
        curSpawnDistance = player.position.x - curEndPos.x;
        curDestroyDistance = Vector3.Distance(player.position, curDestroyPos);
        
        if (levelsFinished)
        {
            if (curSpawnDistance > gameFinishDistance)
            {
                GameManager.instance.WinGame();
            }
        }
        else
        {
            if (curSpawnDistance > spawnDistanceX)
            {
                NextLevelSection();
            }
        }
       
        if (curDestroyDistance > destroyDistanceX)
            UnspawnLevelSection();

    }

    void NextLevelSection()
    {
        if (curSectInd < levels[curLevelInd].levelSections.Count)
            SpawnObstacle(false);
        else if (levels[curLevelInd].timed)
            curSectInd = 0;
        else
            SpawnLastLevelSection();
    }

    void SpawnLastLevelSection()
    {
        SpawnObstacle(true);

        if (curLevelInd < levels.Length - 1)
            curLevelInd++;
        else
            levelsFinished = true;

        ResetIndexes();

        //UI update
    }

    void ResetIndexes()
    {
        curSectInd = 0;
        if (levels[curLevelInd].timed)
            timer = levels[curLevelInd].levelTime;

        //clear random spawn list
        randomSpawns.Clear();
    }

    void SpawnObstacle(bool _end)
    {
        if (_end)
        {
            curSpawn = levels[curLevelInd].endObstacle;
        }
        else
        {
            //get next spawn values
            if (levels[curLevelInd].spawnType == Level.SpawnType.Random)
            {
                curSpawn = GetRandomSpawn();
            }
            else
            {
                curSpawn = levels[curLevelInd].levelSections[curSectInd];
            }
            curSectInd++;
        }

        //store new current spawn values
        Transform spawn = null;
        pool.SpawnObject(curSpawn.gameObject, curEndPos, Quaternion.identity, null, out spawn);
        curEndPos = spawn.GetComponent<LevelSection>().GetEndPoint().position;

        //store spawn in destruction array list
        obsToUnspawn.Add(spawn.gameObject);


    }

    private LevelSection GetRandomSpawn()
    {
        if (curSectInd == 0) //populate random list to start
            randomSpawns = new List<LevelSection>(levels[curLevelInd].levelSections);

        //pick random range
        int rand = Random.Range(0, randomSpawns.Count);
        LevelSection newSpawn = randomSpawns[rand];
        //remove spawn from list so we dont get duplicates
        if (!levels[curLevelInd].timed)
            randomSpawns.Remove(newSpawn);
        //return it
        return newSpawn;
    }

    void UnspawnLevelSection()
    {
        if (obsToUnspawn.Count < 1)
            return;

        pool.UnspawnObject(obsToUnspawn[0]);
        obsToUnspawn.RemoveAt(0);

        //store spawn values for destruction
        curDestroyPos = obsToUnspawn[0].transform.position;
    }
}
