﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{

    [System.Serializable]
    public class SpawnableObject
    {
        [HideInInspector]
        public string name;
        public GameObject prefabToSpawn;
        public int amount = 10;
        [HideInInspector]
        public Transform unspawnParent;
    }

    public List<SpawnableObject> poolObjects = new List<SpawnableObject>();

    private Transform unspawnedObjectsParent;

    void Start()
    {
        CreateSpawnParents();
        PreSpawnObjects();
    }

    void CreateSpawnParents()
    {
        unspawnedObjectsParent = new GameObject().transform;
        unspawnedObjectsParent.SetParent(transform);
        unspawnedObjectsParent.name = "UnspawnedObjects";
    }

    void PreSpawnObjects()
    {
        foreach (var obj in poolObjects)
        {

            obj.unspawnParent = new GameObject().transform;
            obj.unspawnParent.name = obj.prefabToSpawn.name;
            obj.unspawnParent.SetParent(unspawnedObjectsParent);
            obj.name = obj.prefabToSpawn.name;

            for (int i = 0; i < obj.amount; i++)
            {
                var spawn = Instantiate(obj.prefabToSpawn);
                spawn.name = obj.name;
                UnspawnObject(spawn);
            }
        }
    }

    public void SpawnObject(GameObject _obj, Vector3 _pos, Quaternion _rot, Transform _parent, out Transform _spawn)
    {
        SpawnObjectFromPool(_obj, _pos,_rot, _parent, out _spawn);
    }

    public void SpawnObject(GameObject _obj, Vector3 _pos, Quaternion _rot)
    {
        Transform none = null;
        SpawnObjectFromPool(_obj, _pos, _rot, null, out none);
    }

    void SpawnObjectFromPool(GameObject _obj, Vector3 _pos, Quaternion _rot, Transform _parent, out Transform _spawn)
    {
        var parent = FindPoolNameMatch(_obj.name).unspawnParent;
        _spawn = null;
        if (parent.childCount > 0)
        {
            _spawn = parent.GetChild(0);
            _spawn.position = _pos;
            _spawn.rotation = _rot;
            _spawn.SetParent(_parent);
            _spawn.gameObject.SetActive(true);
        }         
        else
        {
            Debug.LogError("No more " + _obj.name + " objects to spawn! Please add more to pool or unspawn other objects before spawning more.");
        }
    }

    public void UnspawnObject(GameObject _obj)
    {
        _obj.SetActive(false);
        _obj.transform.SetParent(FindPoolNameMatch(_obj.name).unspawnParent);
        _obj.transform.SetAsLastSibling();
        _obj.transform.position = Vector3.zero;
        _obj.transform.rotation = Quaternion.identity;
    }

    SpawnableObject FindPoolNameMatch(string _name)
    {
        foreach (var obj in poolObjects)
        {
            if (obj.name == _name)
            {
                return obj;
            }
        }
        Debug.Log("Could not find " + _name + " in Pool. Please add prefab to the pool!");
        return null;   
    }



}
