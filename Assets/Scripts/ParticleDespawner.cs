﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDespawner : MonoBehaviour
{

    private Pool pool;
	private ParticleSystem particle;

	// Use this for initialization
	void Start ()
    {
        pool = GameManager.instance.GetPool();
		particle = GetComponent<ParticleSystem> ();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (!particle.isPlaying)
			pool.UnspawnObject (this.gameObject);
	}
}
