﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{

    [SerializeField]
    private GameObject pauseMenu;
    [SerializeField]
    private bool freezeOnPause;
    private bool paused;

    private float lastTimeScale = 1;

	// Use this for initialization
	void Start ()
    {
        Pause(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetInputs();
	}

    void GetInputs()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            paused = !paused;
            Pause(paused);
        }
    }

    void Pause(bool _pause)
    {
        if (freezeOnPause)
        {
            if (_pause)
            {
                lastTimeScale = Time.timeScale;
                Time.timeScale = 0;
            }     
            else
                Time.timeScale = lastTimeScale;
        }

        pauseMenu.SetActive(_pause);
    }

    public bool IsPaused()
    {
        return paused;
    }
}
